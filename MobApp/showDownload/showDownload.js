
function page1ToPage2(web3) {
    var page1Btn = document.getElementById("page1ButtonA");
    page1Btn.onclick = function () {
        var searchAddress = document.getElementById("searchAddress").value;
        if (searchAddress.length === 42) {
            web3.eth.getCode(searchAddress, function (error, result) {
                if (!error) {
                    if (result === "0x0") {
                        alert("Address exists!");
                        const address = "0x73174393022a9553c1e5500d9cf44d436c5d5aa8";//AderDai address
                        const abi = [
                            {
                                "constant": true,
                                "inputs": [],
                                "name": "name",
                                "outputs": [
                                    {
                                        "name": "",
                                        "type": "string"
                                    }
                                ],
                                "payable": false,
                                "stateMutability": "view",
                                "type": "function"
                            },
                            {
                                "constant": true,
                                "inputs": [],
                                "name": "bidCount_",
                                "outputs": [
                                    {
                                        "name": "",
                                        "type": "uint256"
                                    }
                                ],
                                "payable": false,
                                "stateMutability": "view",
                                "type": "function"
                            },
                            {
                                "constant": true,
                                "inputs": [],
                                "name": "symbol",
                                "outputs": [
                                    {
                                        "name": "",
                                        "type": "string"
                                    }
                                ],
                                "payable": false,
                                "stateMutability": "view",
                                "type": "function"
                            },
                            {
                                "constant": true,
                                "inputs": [
                                    {
                                        "name": "",
                                        "type": "uint256"
                                    }
                                ],
                                "name": "bidder_",
                                "outputs": [
                                    {
                                        "name": "id",
                                        "type": "uint256"
                                    },
                                    {
                                        "name": "addr",
                                        "type": "address"
                                    },
                                    {
                                        "name": "date",
                                        "type": "uint256"
                                    },
                                    {
                                        "name": "frzValue",
                                        "type": "uint256"
                                    },
                                    {
                                        "name": "url",
                                        "type": "string"
                                    },
                                    {
                                        "name": "gen",
                                        "type": "uint256"
                                    },
                                    {
                                        "name": "owner",
                                        "type": "bool"
                                    }
                                ],
                                "payable": false,
                                "stateMutability": "view",
                                "type": "function"
                            },
                            {
                                "constant": true,
                                "inputs": [],
                                "name": "bidPrice_",
                                "outputs": [
                                    {
                                        "name": "",
                                        "type": "uint256"
                                    }
                                ],
                                "payable": false,
                                "stateMutability": "view",
                                "type": "function"
                            },
                            {
                                "constant": true,
                                "inputs": [
                                    {
                                        "name": "",
                                        "type": "address"
                                    }
                                ],
                                "name": "bIDxAddr_",
                                "outputs": [
                                    {
                                        "name": "",
                                        "type": "uint256"
                                    }
                                ],
                                "payable": false,
                                "stateMutability": "view",
                                "type": "function"
                            },
                            {
                                "inputs": [],
                                "payable": false,
                                "stateMutability": "nonpayable",
                                "type": "constructor"
                            },
                            {
                                "constant": false,
                                "inputs": [
                                    {
                                        "name": "_url",
                                        "type": "string"
                                    }
                                ],
                                "name": "uBid",
                                "outputs": [],
                                "payable": true,
                                "stateMutability": "payable",
                                "type": "function"
                            },
                            {
                                "constant": false,
                                "inputs": [
                                    {
                                        "name": "_bID",
                                        "type": "uint256"
                                    }
                                ],
                                "name": "iReturn",
                                "outputs": [],
                                "payable": true,
                                "stateMutability": "payable",
                                "type": "function"
                            },
                            {
                                "constant": false,
                                "inputs": [
                                    {
                                        "name": "_bID",
                                        "type": "uint256"
                                    }
                                ],
                                "name": "iTeam",
                                "outputs": [],
                                "payable": true,
                                "stateMutability": "payable",
                                "type": "function"
                            },
                            {
                                "constant": true,
                                "inputs": [],
                                "name": "getBidPrice",
                                "outputs": [
                                    {
                                        "name": "",
                                        "type": "uint256"
                                    }
                                ],
                                "payable": false,
                                "stateMutability": "view",
                                "type": "function"
                            },
                            {
                                "constant": true,
                                "inputs": [],
                                "name": "getTotalBidder",
                                "outputs": [
                                    {
                                        "name": "",
                                        "type": "uint256"
                                    }
                                ],
                                "payable": false,
                                "stateMutability": "view",
                                "type": "function"
                            },
                            {
                                "constant": true,
                                "inputs": [
                                    {
                                        "name": "_addr",
                                        "type": "address"
                                    }
                                ],
                                "name": "getBidderIdByAddress",
                                "outputs": [
                                    {
                                        "name": "",
                                        "type": "uint256"
                                    }
                                ],
                                "payable": false,
                                "stateMutability": "view",
                                "type": "function"
                            },
                            {
                                "constant": true,
                                "inputs": [
                                    {
                                        "name": "_bID",
                                        "type": "uint256"
                                    }
                                ],
                                "name": "getBidderInfoById",
                                "outputs": [
                                    {
                                        "name": "",
                                        "type": "uint256"
                                    },
                                    {
                                        "name": "",
                                        "type": "address"
                                    },
                                    {
                                        "name": "",
                                        "type": "uint256"
                                    },
                                    {
                                        "name": "",
                                        "type": "uint256"
                                    },
                                    {
                                        "name": "",
                                        "type": "string"
                                    },
                                    {
                                        "name": "",
                                        "type": "uint256"
                                    },
                                    {
                                        "name": "",
                                        "type": "bool"
                                    }
                                ],
                                "payable": false,
                                "stateMutability": "view",
                                "type": "function"
                            },
                            {
                                "constant": true,
                                "inputs": [],
                                "name": "test",
                                "outputs": [
                                    {
                                        "name": "",
                                        "type": "string"
                                    }
                                ],
                                "payable": false,
                                "stateMutability": "view",
                                "type": "function"
                            }
                        ]; //AderDai abi
                        const myContractInstance = web3.eth.contract(abi).at(address);
                        myContractInstance.test.call(function (error, result) {
                            if(!error) {
                                document.getElementById("info").innerText = result;
                            }
                        });
                    }
                    else {
                        alert("No Address!");
                    }
                }
            });
        }
        else {
            alert("Please input correct address!");
        }
    };
}
